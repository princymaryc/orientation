# **Git**

Git is a distributed _version control system_ for tracking changes in source code during software development. It is a software that keeps track on the revision made by the team-mates during the development of your work.
## **Git  vocabulary**
#### 1.Repository
 * collection of code files.
 * big box to throw code.# 
#### 2.GitLab
 * remote storage solution for Git repos.
#### 3.Commit
 * It is like saving your work.
 * It exist on your local machine until it is pushed.
#### 4.Push
 * It is syncing your commits to GitLab.
#### 5.Branch
![Untitled_8](Untitled_8.jpg)
#### 6.Merge
 * Branch free of bug becomes part of primary codebase or Master branch.
 * Integrating 2 branches together.
#### 7.Clone
 * It takes the entire online repository makes and exact copy of it on your local machine.
#### 8.Fork
 * Get an entirely new repo of that code under your own name.

## **TO INSTALL GIT**
 For **linux** open the **terminal** and type
```
sudo apt-get install git

```
## **Git Internals**
Your files can reside in three stages

###### 1.Modified
* you have changed the file but not committed.

###### 2. Staged
* you have marked modified file in its current version.

###### 3. Committed
* data is safely stored in your local repo.
## **GIT WORKFLOW**
![Untitled_9](Untitled_9.jpg)

### **MORE GIT COMMANDS**
```
git init    "to start a new repository "
git diff    "shows the file difference which are not yet staged"
git reset [file]   "unstages the file"
git rm [file]   "deletes the file from your working directory"
git log     "used to list the version history for the current branch"
git branch  "lists all the local branches in the current repository"
```



  

