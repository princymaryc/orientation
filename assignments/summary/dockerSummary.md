# **Docker Terminologies**
### 1. Docker 
* Docker is a software that offers a set of platform as a service products for developing and deploying applications by packaging software in containers.
* Containers are lightweight ,portable,virtual environments that developers can share without risking inconsistencies in development.  

![index](index.png)  
### 2. Docker Image
* A Docker Image contains everything to run an application as a container.
### 3. Container
* One image can create multiple containers.
* A Docker container is a running Docker image.
### 4. Docker Hub
* Docker Hub is like Git Hub for docker images and containers.

# **Docker Installation**
![Untitled_5](Untitled_5.jpg)
# **Docker containers command**
### docker start
* starts any stopped containersID.
### docker stop
* stops any running containers.
### docker run
* create containers from docker image.
### docker rm
* deletes the containers.
# **Docker image command**
### docker build [url]
* create an image from a docker file.
### docker pull [image]
* pull an image from a registry.
### docker push [image]
* push an image to a registry.

# **Common Operations on Docker**
![Untitled_6](Untitled_6.jpg)


 
 


 



